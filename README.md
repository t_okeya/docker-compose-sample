# Prepare Docker for Application Development Environment. #

# Introduction

## Java

* Proxy Server : ./java/docker/nginx
* AP Server    : ./java/docker/tomcat
* DB Server    : ./java/docker/mysql

## PHP

* AP Server   : ./php-httpd/docker/php
* DB Server   : ./php-httpd/docker/mariadb
* SMTP Server : ./php-httpd/docker/smtp

* WEB Server  : ./php-nginx/docker/nginx
* AP Server   : ./php-nginx/docker/php
* DB Server   : ./php-nginx/docker/mysql

# Configuration

## Java

./java/docker-compose.yml

```yml
  ~
  ~
  nginx:
    image: sample/nginx:1.0 -> <changed>/nginx:1.0
  ~
  ~
  tomcat:
    image: sample/tomcat:1.0 -> <changed>/tomcat:1.0
  ~
  ~
  db:
    image: sample/mysql:1.0 -> <changed>/mysql:1.0
```

./java/docker/tomcat/Dockerfile

```yml
ARG JAVA_VERSION="1.8.0"
ARG TOMCAT_MAJOR_VERSION="8"
ARG TOMCAT_VERSION="8.5.54"
ARG MYSQL_CONNECTOR_VERSION="5.1.43"
```

## PHP

### php-httpd

./php-httpd/docker-compose.yml

```yml
  ~
  ~
  web:
    image: sample/php:2.0 -> <changed>/php:2.0
  ~
  ~
  db:
    image: sample/mariadb:2.0 -> <changed>/mariadb:2.0
```

./php-httpd/docker/php/Dockerfile

```yml
ARG php_ver="php72"
ARG php_modules
ARG memory_limit="128M"
ARG upload_max_filesize="2M"
ARG document_root="public"
ARG vhosts
```

### php-nginx

./php-nginx/docker-compose.yml

```yml
  ~
  ~
  web:
    image: sample/nginx:3.0 -> <changed>/nginx:3.0
  ~
  ~
  app:
    image: sample/php:3.0 -> <changed>/php:3.0
  ~
  ~
  db:
    image: sample/mysql:3.0 -> <changed>/mysql:3.0
```

# Usage

execute the following command.

```sh
cd ./java/docker
or
cd ./php-httpd/docker
or
cd ./php-nginx/docker

docker-compose up -d
```
